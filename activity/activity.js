function sumNum(a, b) {
    console.log(a + b);
}

function diffNum(a, b) {
    console.log(a - b);
}

function prodNum(a, b) {
    console.log(a * b);
}

function quoNum(a, b) {
    console.log(a / b);
}

console.log("Sum in Addition");
sumNum(8, 9);
console.log("Difference in Subtraction");
diffNum(6, 3);
console.log("Product in Multiplication")
prodNum(10, 7);
console.log("Quotient in Division");
quoNum(100, 5);